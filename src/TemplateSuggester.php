<?php

namespace Drupal\template_suggester;

use Drupal\Core\Extension\ThemeHandlerInterface;
use Symfony\Component\Yaml\Yaml;

/**
 * Class TemplateSelector.
 */
class TemplateSuggester {

  /**
   * The theme handler.
   *
   * @var \Drupal\Core\Extension\ThemeHandlerInterface
   */
  protected $themeHandler;

  /**
   * Constructs the TemplateSuggester.
   *
   * @param \Drupal\Core\Extension\ThemeHandlerInterface $theme_handler
   *   The theme handler.
   */
  public function __construct(ThemeHandlerInterface $theme_handler) {
    $this->themeHandler = $theme_handler;
  }

  /**
   * Get custom template files.
   *
   * @param string $entity
   *   Entity type: content or paragraph.
   * @param string $bundle
   *   Machine name of the node or paragraph type.
   *
   * @return array
   *   Returns array of template files if they exit.
   */
  public function getTemplateFiles($entity, $bundle) {
    $data                      = [];
    $default_theme             = $this->themeHandler->getDefault();
    $theme_path                = $this->themeHandler->getTheme($default_theme)
      ->getPath();
    $template_suggester_config = $theme_path . '/template_suggester.yml';

    if (file_exists($template_suggester_config)) {
      $templates = Yaml::parse(file_get_contents($template_suggester_config));

      if (!empty($templates[$entity])) {
        foreach ($templates[$entity] as $machine_name => $item) {
          if (in_array($bundle, $item['bundles'])) {
            $data[$machine_name] = $item['name'];
          }
        }
      }
    }

    return $data;
  }

}
