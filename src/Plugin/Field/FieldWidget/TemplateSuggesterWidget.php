<?php

namespace Drupal\template_suggester\Plugin\Field\FieldWidget;

use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\template_suggester\TemplateSuggester;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'template_suggester' widget.
 *
 * @FieldWidget(
 *   id = "template_suggester_widget",
 *   module = "template_suggester",
 *   label = @Translation("Template suggester widget"),
 *   field_types = {
 *     "template_suggester"
 *   }
 * )
 */
class TemplateSuggesterWidget extends WidgetBase implements ContainerFactoryPluginInterface {

  /**
   * Template selector.
   *
   * @var \Drupal\template_suggester\TemplateSuggester
   */
  protected $templateSuggester;

  /**
   * {@inheritdoc}
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, array $third_party_settings, TemplateSuggester $template_suggester) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);
    $this->templateSuggester = $template_suggester;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static($plugin_id, $plugin_definition, $configuration['field_definition'], $configuration['settings'], $configuration['third_party_settings'], $container->get('template_suggester'));
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $template_files = [];

    if ($items->getEntity()->getEntityType() instanceof ContentEntityTypeInterface) {
      $entity = $items->getEntity()->getEntityType()->id();
      $bundle = $items->getEntity()->bundle();
      $template_files = $this->templateSuggester->getTemplateFiles($entity, $bundle);
    }

    $element['value'] = $element + [
      '#type' => 'select',
      '#options' => $template_files,
      "#empty_option" => $this->t('- None -'),
      '#title' => $this->t('Template'),
      '#description' => $this->t('Choose a template'),
      '#default_value' => (isset($items[$delta]->value) && isset($template_files[$items[$delta]->value])) ? $items[$delta]->value : NULL,
    ];
    return $element;
  }

}
