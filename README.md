INTRODUCTION
------------

This module provides a new functionality that let you be able to
extends the core entity templates suggestions with other totally cutomized. 
The module provides a field type named template_suggester where you can select
the new customized suggestion.
It gives you the ability to display one structure display in multiple ways.

REQUIREMENTS
------------
The core field module.

INSTALLATION
------------
Install as you would normally install a contributed Drupal module.
 
CONFIGURATION
-------------

1. Create template_suggester.yml in your active theme folder and add your
   template suggestions (example yml placed in module folder).
2. Add Template Suggester field in to your entity.
3. Add a content and select suggestion as you wish.
4. Once you have a different template for your content
   you have no limits to customize it.

TEMPLATE SUGGESTION LOGIC
-------------
- ENTITY__SELECTED_SUGGESTER
- ENTITY__CURRENT_BUNDLE__SELECTED_SUGGESTER  
- ENTITY__SELECTED_SUGGESTER__VIEW_MODE
- ENTITY__CURRENT_BUNDLE__SELECTED_SUGGESTER__VIEW_MODE

Example yml configuration

```
node: (ENTITY)
  style_1: (SELECTED_SUGGESTER)
    name: Visible name
    bundles:
      - article (CURRENT_BUNDLE)
```

Output

```
node--style-1.html.twig
node--article--style-1.html.twig
node--style-1--full.html.twig
node--article--style-1--full.html.twig
```
---
Tested and works fine with follow entities. 
- node
- taxonomy_term
- paragraph

MAINTAINERS
-----------

Current maintainers:
 * Kushan Gunasinghe - https://www.drupal.org/user/3619158
